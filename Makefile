remove:
	rm -rf venv/

install: remove
	python3 -m venv venv


clear:
	rm *.pyc
	rm -r */*.pyc
	rm -rf __pycache__
